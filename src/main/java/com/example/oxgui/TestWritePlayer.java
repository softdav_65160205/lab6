package com.example.oxgui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author informatics
 */
public class TestWritePlayer {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        Player x = new Player('X');
        Player o = new Player('O');
        System.out.println(x);
        System.out.println(o);
        File file = new File("players.det");
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(o);
        oos.writeObject(x);
        oos.close();
        fos.close();
        
    }

}
